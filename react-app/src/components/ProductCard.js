import {Card, Button} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'


export default function ProductCard({product}){

	

	const {_id, name, description, price} = product;


	return (
		<Card className="my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

				<Link className="btn btn-primary" to={`/products/${_id}`}>View Product</Link>
			</Card.Body>
		</Card>
		)
};

ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}