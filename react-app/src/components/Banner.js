import { Button, Row, Col, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
  return (
    <>
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100 h-60"
            src="https://pauseonline.s3.eu-west-2.amazonaws.com/wp-content/uploads/2020/02/Z_SW_2020_08_Join_the_latest_trend_Showstopper_0236.jpg?w=640"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>NEW YORK</h3>
            <p>The City That Never Sleeps</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 h-60"
            src="https://graziamagazine.com/wp-content/uploads/2022/09/GettyImages-1422688277-Cropped.jpg?fit=6000%2C3375"
            alt="Second slide"
          />
          <Carousel.Caption>
            <h3>LOS ANGELES</h3>
            <p>The City of Angels</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 h-60"
            src="https://media.gq.com/photos/5e207af11644690009342d2f/4:3/pass/GettyImages-1199650911.jpg"
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3>PARIS</h3>
            <p>
              The City of Love
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

        <Row>
        <Col xs={12} md={8} mx-auto className="p-5 text-center">
          <h1>Cities.</h1>
          <p>Stroll the busy streets in style, with Cities., day and night!</p>
            <Button variant="primary" as={Link} to="/products/active">
      Shop Now!
    </Button>
        </Col>
      </Row>
    </>
  );
}