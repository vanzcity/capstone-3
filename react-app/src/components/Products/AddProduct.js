import { useState} from 'react'
import {Button, Form, Modal} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function AddProduct(props){
    const [name,setName] = useState('')
    const [description,setDescription] = useState('')
    const [price,setPrice] = useState(0)

    // States for showing modal
    const [showModal, setShowModal] = useState(false)

    // handle opening and closing of modal
    const openAddModal = () => setShowModal(true)
    const closeAddModal = () => setShowModal(false)

    // handle adding course
    const addProduct = (event) => {
        event.preventDefault()

        // use formdata instead of JSON for multer in backend
        const formData = new FormData()
        const request = {name, description, price}
        console.log(request)
        formData.set('name', 'test')
        formData.set('description', 'description')
        formData.set('price', 3)
        console.log(formData)
        const token = localStorage.getItem("token");
        console.log(process.env.REACT_APP_API_URL)
        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`},
            body: JSON.stringify(request)
        })
        .then(result => {
            if(result) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product added successfully!'
                })
                props.populateProducts()
                closeAddModal()
               
            }
            else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }

            // reset fields after
            setName('')
            setDescription('')
            setPrice(0)
        })
    }

    // const handleImageUpload = (event) => {
    //     const temp_images = [];

    //     [...event.target.files].forEach(file => {
    //         temp_images.push({
    //             file: file,
    //             url: URL.createObjectURL(file)
    //         })
    //     })

    //     setProductImages(temp_images)
    // }

    return(
        <div>
            <Button className="btn-secondary-custom" variant="dark" onClick={openAddModal}>Add New Product</Button>
            
            {/* add modal */}
            <Modal show={showModal} onHide={closeAddModal}>
                <Form onSubmit={ event => addProduct(event) }>
                    <Modal.Header closeButton>
                        <Modal.Title><h4>Add Product</h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
{/*                        <Form.Group className="mb-3">
                            <Form.Label>Product Images</Form.Label>
                            <Form.Control
                                type="file"
                                accept=".jpg,.png,.jpeg"
                                required
                                onChange={e => setProductImage(e.target.files[0])}
                            />
                        </Form.Group>*/}
                        <Form.Group className="mb-3">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                required
                                value={name}
                                onChange={event => setName(event.target.value)}
                                placeholder="Enter Product Name"
                            />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                required
                                value={description}
                                onChange={event => setDescription(event.target.value)}
                                placeholder="Enter Product Description"
                            />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="text"
                                required
                                value={price}
                                onChange={event => setPrice(event.target.value)}
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAddModal}>Close</Button>
                            <Button className="btn-secondary-custom" variant="dark" type="submit">
                            Submit
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    )
}