import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({ productId, isActive, refreshData}) {
	const archiveToggle = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: 'PATCH',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data =>{
			if(data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled!'
				})
				refreshData()
			}else {
				Swal.fire({
					title: 'error',
					icon: 'error',
					text: 'Something went wrong'
				})
				refreshData()
			}
		})
	}

	const activateToggle = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update/active`,{
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				"isActive": true
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully activated!'
				})
				refreshData()
			}else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Something went wrong'
				})
				refreshData()
			}
		})
	}

	return(
		<div>
			{ isActive  ?
				<Button variant="danger" onClick={() => archiveToggle(productId)}>
					Disable
				</Button>

				:

				<Button className="btn-secondary-custom" variant="dark" onClick={() => activateToggle(productId)}>
					Enable
				</Button>

			}
			</div>
		)
}