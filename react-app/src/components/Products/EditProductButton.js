import React, {useState} from 'react'
import {Button, Form, Modal} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function EditProductButton(props){
    const [name,setName] = useState(props.product.name)
    const [description,setDescription] = useState(props.product.description)
    const [price,setPrice] = useState(props.product.price)

    // States for showing modal
    const [showModal, setShowModal] = useState(false)

    // handle opening and closing of modal
    const openAddModal = () => setShowModal(true)
    const closeAddModal = () => setShowModal(false)

    const editProduct = (event) => {
        event.preventDefault()
        const request = {name, description, price}
        const formData = new FormData()

        formData.append('name', name)
        formData.append('description', description)
        formData.append('price', price)

        fetch(`${process.env.REACT_APP_API_URL}/products/${props.product._id}/update`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(request)
        })
        .then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product successfully updated!'
                })

                props.populateProducts()
                closeAddModal()
            }
            else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Something went wrong. Please try again.'
                })
            }
        })
    }

    return(
        <div>
            <Button className="btn-secondary-custom" variant="dark" onClick={openAddModal}>Edit</Button>
            {/* edit modal */}
            <Modal show={showModal} onHide={closeAddModal}>
                <Form onSubmit={ event => editProduct(event) }>
                    <Modal.Header closeButton>
                        <Modal.Title><h4>Edit Product</h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                required
                                value={name}
                                onChange={event => setName(event.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                required
                                value={description}
                                onChange={event => setDescription(event.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className='mb-3'>
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="text"
                                required
                                value={price}
                                onChange={event => setPrice(event.target.value)}
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAddModal}>Close</Button>
                          <Button className="btn-secondary-custom" variant="dark" type="submit">
                            Submit
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    )
}