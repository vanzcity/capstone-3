import {Navbar, Container, Nav, NavDropdown} from 'react-bootstrap'
import {Fragment, useContext} from 'react'
import {Link, NavLink} from 'react-router-dom'
import UserContext from '../UserContext' 

function AppNavbar() {

  const {user} = useContext(UserContext)

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
    <Container>
    <Navbar.Brand as={Link} to="/">Cities.</Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
    {user.isAdmin === true ? (
      <>
      <Nav.Link as={NavLink} to="/products/active">
      Products
      </Nav.Link>
      <Nav.Link as={NavLink} to="/admin/dashboard">
      Dashboard
      </Nav.Link>
      </>
      ) : (
      <Nav.Link as={NavLink} to="/products/active">
      Products
      </Nav.Link>
      )}
      </Nav>
      <Nav>
      { user.id !== null ? (
        <Nav.Link as={NavLink} to="/logout">
        Log Out
        </Nav.Link>
        ) : (
        <>
        <Nav.Link as={NavLink} to="/login">
        Log In
        </Nav.Link>
        <Nav.Link as={NavLink} to="/register">
        Register
        </Nav.Link>
        </>
        )}
        </Nav>
        </Navbar.Collapse>
        </Container>
        </Navbar>
        )
}

export default AppNavbar