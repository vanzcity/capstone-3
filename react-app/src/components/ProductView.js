import {useState, useEffect, useContext} from 'react'
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView(){
	
	const {user} = useContext(UserContext)

	// Get the value of the courseId from the URL parameters (/courses/:courseId/view)
	const {productId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const navigate = useNavigate();

	const checkout = (productId) => {
		console.log(productId)
		console.log(name)
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if (typeof result.message !== "undefined") {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: result.message
				})
				navigate("/products/active")
			}
		}).catch(error => {
			Swal.fire({
				title: "Oopsie daisy",
				icon: "error",
				text: "Something went wrong :("
			})
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,
			{method: "GET"})
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])

	return (

		<Container className="mt-5">
			<Row>

			<Col lg={{span: 6, offset: 3}}>
				<Card className="my-3">
				<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

				{user.id !== null ? 
				<Button disabled={user.isAdmin===true} variant="primary" onClick={() => checkout(productId)}>Buy Now</Button>
					:
					<Link className="btn btn-warning" to="/login">Log In to Purchase</Link>
			}
			</Card.Body>
			</Card>
			</Col>
			</Row>
			</Container>

		)

}