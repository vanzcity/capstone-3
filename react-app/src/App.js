import { useState, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import AppNavbar from "./components/AppNavbar.js";
import ProductView from "./components/ProductView.js";
import { Container } from "react-bootstrap";
import Register from "./pages/Register.js";
import AdminDashboard from "./pages/AdminDashboard.js";
import Home from "./pages/Home.js";
import Login from "./pages/Login.js";
import Logout from "./pages/Logout.js";
import Products from "./pages/Products.js";
import { UserProvider } from "./UserContext.js";
import { Route, Routes } from "react-router-dom";
import "./App.css";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    token: "",
  });

  useEffect(() => {
    console.log("Environment: ", process.env.NODE_ENV);
  }, []);

  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/products/active" element={<Products />} />
            <Route path="/admin/dashboard" element={<AdminDashboard />} />
            <Route path="/products/:productId" element={<ProductView />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
