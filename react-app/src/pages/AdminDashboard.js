import { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import AddProduct from "../components/Products/AddProduct.js";
import EditProductButton from "../components/Products/EditProductButton.js";
import ArchiveProductButton from "../components/Products/ArchiveProductButton.js";

export default function AdminDashboard(props) {
  const [products, setProducts] = useState([]);
  const populateProducts = () => {
    const token = localStorage.getItem("token");
    const headers = { Authorization: `Bearer ${token}` };
    fetch(`${process.env.REACT_APP_API_URL}/products/`, { headers })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        setProducts(result);
      });
  };

  useEffect(() => {populateProducts()}, []);

  return (
    <div className="mt-5">
      <div className="text-center my-4">
        <h1>Admin Dashboard</h1>
        <AddProduct populateProducts={populateProducts}/>
      </div>

      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colspan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product._id}>
              <td>{product._id}</td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td className={product.isActive ? "text-success" : "text-danger"}>
                {product.isActive ? "In Stock" : "Out of Stock"}
              </td>
              <td>
                <EditProductButton product={product} populateProducts={populateProducts}/>
              </td>
              <td>
                <ArchiveProductButton
                  productId={product._id}
                  isActive={product.isActive}
                  refreshData={populateProducts}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
