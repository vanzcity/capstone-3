import {useState, useEffect} from 'react'
import ProductCard from '../components/ProductCard'
import {Row} from 'react-bootstrap'
import AdminDashboard from './AdminDashboard'

export default function Products(){

	const [products, setProducts] = useState([])
	const [user, setUser] = useState()

	useEffect(() => {
		const token = localStorage.getItem("token");
		const headers = {'Authorization': `Bearer ${token}`}
		fetch(`${process.env.REACT_APP_API_URL}/products/active`, {headers})
		.then(response => response.json())
		.then(result => {
			console.log(result)
		setProducts(result.map(product => {
				return(
					<ProductCard key={product._id} product={product}/>
					)
			}))
		})
	}, [])

	return (
	<Row>
		{products}
		</Row>
		)
}