import {Form, Button,Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

const navigate = useNavigate()

const {user} = useContext(UserContext)
const [email, setEmail] = useState('')
const [password1, setPassword1] = useState('')
const [password2, setPassword2] = useState('')
const [isActive, setIsActive] = useState(false)

function registerUser(event) {
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/check-email`,{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			email: email
		})
	})
	.then(response => response.json())
	.then(result => {
		if(result === true){
			Swal.fire({
					title: "User Already exists!",
					icon: "error",
					text: "The email you provided already exists in the server! Provide another email for registration."
				})

		} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					if(typeof result !== "undefined"){
						// For clearing out the form
						setEmail('')
						setPassword1('')
						setPassword2('')

						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Start shopping!"
						})

						navigate("/login")
					}
				}).catch(error => {
					Swal.fire({
						title: "Something went wrong :(",
						icon: "error",
						text: error.message
					})
				})
			}
		})
	}

	useEffect(() => {
	if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password1, password2])


return(

		// (user.id !== null) ?

		// 	// <Navigate to="/courses"/>

		// :

		<Row>
		<Col>
		<Form onSubmit={(event) => registerUser(event)}>
					<h1>Register</h1>

		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email"
			                value = {email}
			                 onChange ={event => setEmail(event.target.value)}
			                required
		                />

		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value = {password1}
			                onChange ={event => setPassword1(event.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="password2">
		                <Form.Label>Verify Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                 onChange ={event => setPassword2(event.target.value)}
			                required
		                />
		            </Form.Group>


		            { isActive ? 
		                <Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>
		            :
		                <Button disabled variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>

		            }
		        
		        </Form>
		        </Col>
		        </Row>
		)
}