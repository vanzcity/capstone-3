import {Form, Button, Row, Col} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){

const {user, setUser} = useContext(UserContext)
const [email, setEmail] = useState('')
const [password1, setPassword1] = useState('')
const [isActive, setIsActive] = useState(false)

function loginUser(event) {
	event.preventDefault()
	fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password1
		})
	})
	.then(response=> response.json())
	.then(result => {
		console.log(result)
		if(result.accessToken){
				localStorage.setItem('token', result.accessToken)
				retrieveUserDetails(result.accessToken)

					setEmail('')
					setPassword1('')

			/*// show an alert
			alert("Congratulations for logging in!!")*/

			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to our city!"
			})

	}
}).catch(error => {
			// Show an error alert
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Kindly check your login credentials."
				})
		})	
}

const retrieveUserDetails = (token) => {
	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(response => response.json())
	.then(result => {
		setUser({
			id: result._id,
			isAdmin: result.isAdmin,
			token
		})
	})
}

useEffect(() => {
	if ((email !== '' && password1 !== '')){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password1])

	return(
(user.isAdmin && user.id !== null) ? 
      <Navigate to="/admin/dashboard" />
     : (
      !user.isAdmin && user.id !== null) ? 
        <Navigate to="/products/active" />
       : 
		<Row>
		<Col>

		<Form onSubmit={(event) => loginUser(event)}>
					<h1>Log In</h1>

		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email"
			                value = {email}
			                onChange ={event => setEmail(event.target.value)} 
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value = {password1}
			                onChange ={event => setPassword1(event.target.value)}
			                required
		                />
		            </Form.Group>

		 


		            { isActive ? 
		                <Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>

		            :
		                <Button disabled variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>

		            }
		        
		        </Form>

		        </Col>
		        </Row>
		)

}